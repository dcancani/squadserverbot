import boto3
import paramiko
from pprint import pprint
from Config import *

class AWS:
    def __init__(self):
        self.awsConfig = get_config()["aws"]
        self.ec2 = boto3.resource('ec2',
            region_name=self.awsConfig["working-zone"],
            aws_access_key_id=self.awsConfig["key"],
            aws_secret_access_key=self.awsConfig["token"]
        )

    def GetInstances(self):
        response = self.ec2.instances.all()
        return response

    def StartInstance(self,instanceId):
        response = self.ec2.Instance(instanceId).start()
        return response

    def StopInstance(self,instanceId):
        response = self.ec2.Instance(instanceId).stop()
        return response

    def RebootInstance(self,instanceId):
        response = self.ec2.Instance(instanceId).reboot()
        return response

    #Defines if an instance is in a consistent state to be able to receive actions. For example you cannot start an instance in "stopping" state
    def ConsistentState(self,instanceId):
        consistentState = ['running', 'stopped']
        response = self.ec2.Instance(instanceId)
        if response.state['Name'] in consistentState :
            return True
        else:
            return False

class SSH:
    def __init__(self,):
        self.gameServerConfig = get_config()["gameServers"]

    # Commands must be an array
    def sshConnection(self,gameServerName,istanceIp,commands):
        username = self.gameServerConfig[gameServerName]['username']
        keyPath = './Credentials/pemFiles/' + self.gameServerConfig[gameServerName]["pemKey"]
        host = istanceIp
        print(username,keyPath,host)
        key = paramiko.RSAKey.from_private_key_file(keyPath)
        print(key)
        ssh = paramiko.client.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(host, username=username, pkey=key)
        for command in commands:
            print(command)
            _stdin, _stdout,_stderr = ssh.exec_command(command)
            print(_stdout.read().decode())
        ssh.close()

    def gitPull(self,gameServerName,istanceIp):
        gitEnable = self.gameServerConfig[gameServerName]['git']['enable']
        print(gitEnable)
        if gitEnable:
            print('GIT gameserver\'s configs pull enabled')
            commands = self.gameServerConfig[gameServerName]['git']['commands']
            self.sshConnection(gameServerName,istanceIp,commands)
        else:
            print('GIT gameserver\'s configs pull disabled')
            return False

    def serverConfigApply(self,gameServerName,istanceIp):
            print('Apply new config to server')
            commands = self.gameServerConfig[gameServerName]['configApply']['commands']
            self.sshConnection(gameServerName,istanceIp,commands)
