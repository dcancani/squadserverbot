import discord
import random
from discord.ext.commands import Bot
from discord.ext import commands, tasks
from pprint import pprint
from UIElements import *
from ManageServer import *

config = get_config()["discord"]
discordToken=config["token"]
prefix = config["prefix"]

intents = discord.Intents.default()
intents.message_content = True
activity = discord.Game(name="Greve")
bot = commands.Bot(command_prefix= prefix,intents=intents,activity = activity)
AWS = AWS()


@bot.event
async def on_ready():
    change_activity.start()
    print("We have logged in as {0.user}".format(bot))

@bot.command()
async def start(ctx):
    await ctx.send("```Start starta, starta sto cazzo```")


@bot.command()
async def menu(ctx):
    instances = AWS.GetInstances()
    view = InstancesListView(instances=instances)
    await ctx.send(embed = InstanceListEmbed(instances=instances), view = view)


@bot.command()
async def status(ctx):
    instances = AWS.GetInstances()
    for instance in instances:
        pprint(instance.tags[0]['Value'])
        embed=discord.Embed(title=instance.tags[0]['Value'], url="", color=discord.Color.blue())
        embed.set_thumbnail(url="https://logos-world.net/wp-content/uploads/2021/08/Amazon-Web-Services-AWS-Logo.png")
        embed.add_field(name="ID", value=instance.id, inline=False)
        embed.add_field(name= "Public IPv4",value=instance.public_ip_address,inline=False)
        embed.add_field(name="State", value= instance.state['Name'],inline=False)
        embed.set_footer(text = "basta che non piangi")
        await ctx.send(embed=embed)

@tasks.loop(minutes=30)
async def change_activity():
    await bot.change_presence(activity=discord.Game(random.choice(list(config["activities"].values()))))


bot.run(discordToken)
