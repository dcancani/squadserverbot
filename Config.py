import yaml

with open("Credentials/bot-config.yml","r") as f:
    config = yaml.load(f,Loader=yaml.FullLoader)
    f.close()

def get_config():
    return config