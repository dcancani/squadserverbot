# SquadServerBot
This Discord BOT is capable of starting, stopping and configuring a SQUAD game server.

## Packages
- PyYAML
- paramiko


## Getting started
Rename the files in the `Credentials` folder by changing `.example` with `.yml`

Insert your data in place of the comments. Remember that the order of the values is important.

Put your .pem files in `Credentials/pemFiles/` folder and remember chmod 400 permissions set

Move to the root of the project and run `$ python3 SquadServerBot.py`
to start the bot.


## License
Do what you want with it, I'll be happy.

