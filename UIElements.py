import discord
from discord import *
from discord.ext import commands
from discord.ui import *
from ManageServer import *
from pprint import pprint

AWS = AWS()

#-----PAGINA 1 MENU ISTANZE----

#View per selezionare le varie istanze
class InstancesListView(View):
    def __init__(self, instances):
        super().__init__()
        for instance in instances:
            super().add_item(InstanceButton(label = instance.tags[0]['Value'], instance = instance))

#Embed per visualizzare tutte le istanze
class InstanceListEmbed(Embed):
    def __init__(self, instances):
        super().__init__(title="Clicca sull'istanza che vuoi gestire", url="", color=discord.Color.blue())
        super().set_thumbnail(url="https://logos-world.net/wp-content/uploads/2021/08/Amazon-Web-Services-AWS-Logo.png")
        super().set_footer(text = "basta che non piangi")
        for instance in instances:
            super().add_field(name="Nome", value=instance.tags[0]['Value'], inline=False)

#Bottone per selezionare la singola istanza
class InstanceButton(Button):
    instanceB = ""
    def __init__(self, label, instance):
        self.instanceB = instance
        super().__init__(label = label, style=discord.ButtonStyle.blurple)

    async def callback(self, interaction):
        await interaction.response.edit_message(embed=IstanceEmbed(instance=self.instanceB), view= InstanceView(instance=self.instanceB))

#-----PAGINA 2 MENU ISTANZA----

#Embed per visualizzare la singola istanza
class IstanceEmbed(Embed):
    def __init__(self, instance):
        super().__init__(title=instance.tags[0]['Value'], url="", color=discord.Color.blue())
        super().set_thumbnail(url="https://images.emojiterra.com/google/android-10/512px/267f.png")
        super().add_field(name="ID", value=instance.id, inline=False)
        super().add_field(name= "Public IPv4",value=instance.public_ip_address,inline=False)
        super().add_field(name="State", value= instance.state['Name'],inline=False)
        super().set_footer(text = "basta che non piangi")

class StartButton(Button):
    instanceB = ""
    def __init__(self, instance):
        self.instanceB = instance
        super().__init__(label="Start", style= ButtonStyle.success, emoji="♿")
    
    async def callback(self, interaction):
        response = AWS.StartInstance(self.instanceB.id)
        print(response)
        self.instanceB.state['Name'] = "Starting"
        await interaction.response.edit_message(embed=IstanceEmbed(instance=self.instanceB), view= None)
        #QUA CHIAMI LA FUNZIONE DI AWS passando come parametro self.instanceB

class StopButton(Button):
    instanceB = ""
    def __init__(self, instance):
        self.instanceB = instance
        super().__init__(label="Stop", style= ButtonStyle.danger, emoji="👬")
    
    async def callback(self, interaction):
        response = AWS.StopInstance(self.instanceB.id)
        print(response)
        self.instanceB.state['Name'] = "Stopping"
        await interaction.response.edit_message(embed=IstanceEmbed(instance=self.instanceB), view= None)

class RestartButton(Button):
    instanceB = ""
    def __init__(self, instance):
        self.instanceB = instance
        super().__init__(label="Restart", style= ButtonStyle.grey, emoji="🔯")
    
    async def callback(self, interaction):
        response = AWS.RebootInstance(self.instanceB.id)
        self.instanceB.state['Name'] = "Restarting"
        await interaction.response.edit_message(embed=IstanceEmbed(instance=self.instanceB), view= None)

class InstanceView(View):
    def __init__(self, instance):
        super().__init__()
        super().add_item(StartButton(instance=instance))
        super().add_item(StopButton(instance=instance))
        super().add_item(RestartButton(instance=instance))
    


